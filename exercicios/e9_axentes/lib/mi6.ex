defmodule Mi6 do

use GenServer
use Agent

 # Client

 def fundar do
   GenServer.start_link(Mi6, Db.new, name: :mi6)
 end

 def recrutar(ax,des) do
   GenServer.cast(:mi6,{:recruit,ax,des})
 end


 def asignar_mision(ax,mis) do
   GenServer.cast(:mi6,{:assign,ax,mis})
 end

 def consultar_estado(ax) do
   GenServer.call(:mi6,{:consult,ax})
 end


 def disolver do
   GenServer.call(:mi6,{:stop})
   GenServer.stop(:mi6)
 end


 # Server (callbacks)
 @impl true
 def init(db) do
   {:ok, db}
 end

 def start_link(agent,des) do
    Agent.start_link(fn -> Enum.shuffle(Create.create(String.length(des)))  end, name: agent)
  end


 @impl true
 def handle_cast({:recruit, ax,des}, db) do
   case (start_link(ax,des)) do
     {:ok,e} -> db = Db.write(db,ax,e)
                {:noreply, db}
     {:error,_} -> {:noreply, db}
   end

 end

 @impl true
 def handle_cast({:check, _,_}, db) do
    IO.inspect db
   {:noreply, db}
 end

 @impl true
 def handle_cast({:assign, ax,:espiar}, db) do
   case Db.read(db,ax) do
     {:ok, agente} -> Agent.update(agente, fn [h|t] -> Manipulating.filter([h|t], h) end)
     _ -> :ok
   end
     {:noreply, db}
 end

 @impl true
 def handle_cast({:assign, ax,:contrainformar}, db) do
   case Db.read(db,ax) do
     {:ok, agente} -> Agent.update(agente, fn l -> Manipulating.reverse(l) end)
     _ -> :ok
   end
     {:noreply, db}
 end

 @impl true
 def handle_call({:consult,ax}, _from, db) do
      res = Db.read(db,ax)
      case res do
        {:ok,l} -> {:reply, l|> Agent.get(fn x -> x end),db}
        _ -> {:reply, :you_are_here_we_are_not,db}
      end
 end

 @impl true
 def handle_call({:stop}, _from, db) do
   Enum.map(db, fn {_,e} -> Agent.stop(e) end)
   {:reply,:ok,Db.destroy(db)}
 end

end
