defmodule Create do

def create(n) do
  case n do
    _ when (n != 0) -> for x <- 1..n, do: x
    _ -> []
  end

end

def reverse_create(n) do
  case n do
    _ when (n != 0) ->  for x <- n..1, do: x
    _ -> []
  end
end


end
