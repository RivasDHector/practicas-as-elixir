defmodule Ring do

def start(n,m,msg) do
  first = spawn(fn -> nodo() end)

  crear(n-1,m,msg,[first],n)
  :ok
end


defp crear(n,m,msg,l,no) do
  case n do
    0 -> mandar(no,m,msg,Enum.reverse(l))
    _ -> pid = spawn(fn -> nodo() end)
         crear(n-1,m,msg,[pid|l],no)
  end
end


defp mandar(no,m,msg,l) do
  send hd(l),{[m-1,no,l],msg}
end

def siguiente(pid,l,lt) do
  case l do
    [_|[]] -> hd lt
    [h1|[h2|_]] when pid == h1 -> h2
    _ -> siguiente(pid,tl(l),lt)
  end
end

defp nodo() do
  receive do
    {[0,n,l],_} -> send hd(l),{n,l}
                   nodo()
    {[m,n,l],msg} -> next = siguiente(self(),l,l)
      IO.puts msg
      send next,{[m-1,n,l],msg}
      nodo()
    {1,_} ->
        Process.exit(self(),:normal)
    {n,l} ->
      next = siguiente(self(),l,l)
      send next, {n-1,l}
      Process.exit(self(),:normal)

  end
end

end
