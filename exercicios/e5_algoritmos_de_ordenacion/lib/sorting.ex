defmodule Sorting do

  def quicksort([]) do
      []
  end
  def quicksort([h|t]) do
    {menores, mayores} = Enum.split_with(t, fn x -> (x < h) end)
    quicksort(menores) ++ [h] ++ quicksort(mayores)
  end

defp merge(l1,l2) do
    merge(l1,l2,[])
end


defp merge([],[],res) do Enum.reverse res
end
defp merge([h|t],[],res) do
  merge(t,[],[h|res])
end
defp merge([],[h|t],res) do
  merge([],t,[h|res])
end
defp merge([h1|t1],[h2|t2],res) do
  if (h1 <= h2) do
    merge(t1,[h2|t2],[h1|res])
  else
    merge([h1|t1],t2,[h2|res])
  end
end

  def mergesort ([]) do
    []
  end
  def mergesort([l]) do
    [l]
  end
    def mergesort(list) do
        {l, r} = Enum.split(list, div(length(list), 2))
        merge(mergesort(l), mergesort(r))
    end


end
