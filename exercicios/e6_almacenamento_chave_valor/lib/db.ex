defmodule Db do

  def new do
    []
  end

  defp existe(k,l) do
    case l do
      [] -> false
      [{key,_}|_] when key == k -> true
      [_|t] -> existe(k,t)
    end
  end

  def write([],k,e) do
    [{k,e}]
  end
  def write(l,k,e) do
    ex = existe(k,l)
    case ex do
      true -> write_aux(l,k,e,[])
      false -> [{k,e}|l]
    end
  end

  defp write_aux(l,k,e,res) do
    case l do
      [] -> Enum.reverse res
      [{key,_}|[]] when key == k -> Enum.reverse [{k,e}|res]
      [{key,_}|t] when key == k -> Enum.reverse [{k,e}|[t|res]]
      [h|t] -> write_aux(t,k,e,[h|res])
    end
  end

  def delete([],_) do
    []
  end
  def delete([{k,e}|t],key) do
    delete([{k,e}|t],key,[])
  end

  def delete([{k,e}|t],key,res) do
    case key do
      _ when k == key -> [t|res]
      _ -> delete(t,key,[{k,e}|res])
    end
  end


  def read([],_) do
    {:error, :not_found}
  end
  def read([{k,e}|t],key) do
    case key do
      _ when k == key -> {:ok,e}
      _ -> read(t,key)
    end
  end
  def read(_,_) do
    {:error, :not_found}
  end



  def match([],_) do
    []
  end
  def match([{k,e}|t],ele) do
    match([{k,e}|t],ele,[])
  end

  def match([],_,res) do
    res
  end
  def match([{k,e}|t],ele,res) do
    case e do
      _ when e == ele -> match(t,ele,[k|res])
      _ -> match(t,ele,res)
    end
  end


  def destroy(_) do
    :ok
  end


end
