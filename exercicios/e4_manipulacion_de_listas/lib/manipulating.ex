defmodule Manipulating do

  def filter(l,n) do
    for x <- l, x <= n, do: x
  end

  def reverse l do
    reverse(l,[])
  end

  defp reverse(l,v) do
    case l do
      [] -> v
      [h|t] -> reverse(t, ([h|v]))
    end
  end

  defp recorrer(h,res) do
    case h do
      [] -> res
      [h|t] -> recorrer(t,[h|res])
    end
  end


  defp concatenate(l,res) do
    case l do
      [h|t] -> concatenate(t, recorrer(h,res))
      [] -> reverse res
    end
  end

  def concatenate ([]) do
    []
  end
  def concatenate l do
    concatenate(l,[])
  end


  def flatten(l,res) do
    case l do
      [[]|t] -> flatten(t,res)
      [h|t] when is_list(h) -> flatten(t,flatten(h,res))
      [h|t] -> flatten(t,[h|res])
      [] -> res
    end
  end


  def flatten l do
    reverse (flatten(l,[]))
  end

end
